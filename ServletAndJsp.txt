if the client request a page which is dynamic then that one was build on run time using helper application

Helper application also called web container. In web container there are servelet.The servelet are java class which perform some task.

Take request from the client and process the request and gave response on a html/json/xml/string format

some webcontainer- apache tomcat, glass fish, ibm websphere.

In webcontainer there are deployment descriptor(web.xml) which employ for which request which servlet to be called.

Can create a servelet by extending HttpServlet to the normal class in java

In servelet 3.0 we can create replace web.xml with the help of Annotations(we don't have to create web.xml).

if we create a html page which can accept and do the sum operation then we should use a servlet. we can crate a servlet by extending HttpServlet
and inside the class there should be a method named service which do the addition operation

This service method can accept the request from the user and send the response

First we have to configure the servlet with the class so that it know which class is associated to which servlet

Request vs Response
--------------------
User sends information to the server in the object called request.  

And server sends information to the user in the object called response.this reponse object can be text html/xml/video/image


Get vs Post
------------
get- by default get. all the query appear on the address bar. Should use when fetching data from server
post- all the query not put in address bar. Should use when sending important/private message to server

if we want the work on the request which is only "POST" request. Then we can use doPost method insted of service method. this doPost is given by servlet.
by default every servlet call service method, inside service it check the req.getMethod=="POST" if true then call doPost method.



RequestDispatcher| Calling a servlet from a servlet
----------------------------------------------------
can send data from one servlet to other using session management.

RequestDispatcher is a interface we can create object of RequestDispatcher using req.getRequestDispatcher("url_pattern_name")
and forward ther req and resp to that object.

easiest way to do this is by modifying the request object as we are sending the request to the other servlet and we can retrive the data from the request object

can send data to servlet by using .setAttribute("variableName",variable) and retrive from the other servlet by .getAttribute("variableName")

user can not know where does he get the response is it from the servlet1 or is it from the servlet 2.


SendRedirect
------------
In request dispatcher the user can not know what is happening inside. But in this servlet 1 inform servlet 2.

It is done when going from onesite to other. like while buying a dress if you click on buy it is redirected to paytm/any other payment gateway.

In redirect, 2 different request object is used one for servlet1 and other for servlet2.

servlet1 get the request1 and send the response1 to the browser informing the redirect then redirect occurs with request2 and send a response2

In this can send the data from one servlet to other by using session management.

we can pass the parameter/data by rewriting the url because in get method all the parameter are passed in the url and by rewriting we can access value in the another servlet.

res.sendRedirect("square?k="+k)
here square is the url of the second servlet and the value we are passing is k
we can access k in the other servlet just like we can access other parameter in servlet.
by using req.getParameter (the output of this is string we need to convert it to int if the value we want is integer).

This is not good idea if we want to pass the parameter into multiple servlet

SESSION-

The better approach would be using session. In session we set the value and when ever we need the value we can use it from the session.

HttpSession session =req.getSession() first we have to get the session object and then 
we can use session.setAttribute and session.getAttribute to send and recieve the value. (use it to maintain login).  

COOKIE-

we can set value in cookie also.
can create cookie by creating a cookie object 
Cookie cookie=new Cookies("key",value);
value must be a  string also.
 

ServletConfig and ServletContext
---------------------------------

to get initial value for the servlet

servlet context for all the servlet while servletconfig for individual servlet.



* servlet context is declare in .xml file as-

<context-param>
	<param-name>name</param-name>
	<param-value>Subhasis</param-value>
</context-param>


servlet can access it using-

ServletContext cs=getServletContext();/ can also use req.getServletContext();
String str=cs.getInitParameter("name")



* to use servletConfig

declare config in .xml file as-

inside the specific servlet tag
<init-param>
	<param-name>name</param-name>
	<param-value>Subhasis</param-value>
</init-param>

can access it using-

ServletConfig cg=getServletConfig();/ can also use req.getServletConfig();
String str=cg.getInitParameter("name")

----------
Instead of using .xml file and create servlet tag and map those tag . we can use simple annotation @WebServlet("/add") above the class and @WebServlet("/square").

If our intention is to process the data then servlet is better . if our intention is to show the page then we use jpa.

-----------------------------------------------------------------------------JSP---------------------------------------------------------------------------------------------------
in jsp we write java code inside html code.
because it is hard to create a fully customised page in servlet.

we write java code inside <% ------logic-------%>  - this sign is called as scriplet.
every thing inside this sign goes to the service method in servlet.

and inside the html page(in our example while submiting form we specify the action="add.jsp")

we don't have to create a reference of HttpServletRequest and HttpServletResponse. it is automatically done. We don't have to create any object like we do in servlet.
can use directly request.getParameter("first")
and print value like out.println() with out creating object of out.
out,session,cookie every object in servlet is given by jsp
these are called implicit object

automatically jsp is converted to servlet . Because tomcat work on servlet. if we remove the servlet class and use jsp it works.That mean it somewhere converting servlet to jsp.


1. <% ----logic---%>   this is called scriplet. anything inside this is equivalent to code we write in service method.
2. <%!  declaration %>   this is called declaration. anything we declare inside the class we can do inside this tag.
3. <%@  page import="java.util.array","java.sql" %> this is called directive. all the import packages are separed by comma.
4. <%= printstatement %> this is called expression. whatever we write in this it is goes to out.println()





























